package com.example.appgui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Grille calculatrice simple avec space
        //setContentView(R.layout.grille);

        // Grille calculatrice simple avec colspan
        //setContentView(R.layout.grillebis);

        // Grille linear avec poids
        //setContentView(R.layout.linear);


        // Grille linearBis avec poids
        //setContentView(R.layout.linearbis);


        // Grille table avec poids
        //setContentView(R.layout.form);
    }
}